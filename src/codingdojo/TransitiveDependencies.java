package codingdojo;

import java.util.*;

public class TransitiveDependencies {

    Map<String, String[]> graph = new HashMap<>();

    public void addDependency(String s, String... deps) {
        graph.put(s, deps);
    }

    public String getTransitiveDependencies(String a) {
        return String.join(" ", getTransitiveDependenciesAsSet(a));
    }

    private Set<String> getTransitiveDependenciesAsSet(String s) {
        String[] elements = graph.getOrDefault(s,new String[]{});
        Set<String> result = new HashSet<>(Arrays.asList(elements));

        for (String element : elements) {
            result.addAll(getTransitiveDependenciesAsSet(element));
        }

        return result;
    }
}
