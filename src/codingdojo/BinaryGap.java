package codingdojo;

public class BinaryGap {
    public int getGap(int number) {
        String binaryString = Integer.toBinaryString(number);

        int currentMax = 0;
        int currentSequence = 0;

        for (char s : binaryString.toCharArray()) {
            if ('0' == s) {
                currentSequence++;
            } else {
                if (currentMax < currentSequence) {
                    currentMax = currentSequence;
                }
                currentSequence = 0;
            }


        }

        return currentMax;
    }
}
