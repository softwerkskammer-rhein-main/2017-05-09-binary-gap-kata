package codingdojo;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinaryGapTest {

    BinaryGap testee = new BinaryGap();


    @Test
    public void name() {
        assertEquals(0, testee.getGap(1));
        assertEquals(0, testee.getGap(3));
        assertEquals(1, testee.getGap(5));
        assertEquals(2, testee.getGap(9));
        assertEquals( 1, testee.getGap( 10));
        assertEquals(0,testee.getGap(255));
        assertEquals( 2,testee.getGap(37));
        assertEquals( 1, testee.getGap(40));
    }
}
