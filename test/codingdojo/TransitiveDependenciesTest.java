package codingdojo;

import org.junit.Test;

import static org.junit.Assert.*;

public class TransitiveDependenciesTest {

    @Test
    public void simpleTestcase() {
        TransitiveDependencies testee = new TransitiveDependencies();
        testee.addDependency("A", "B", "C");
        testee.addDependency("B", "C");
        testee.addDependency("C", "D");
        testee.addDependency("D", "E", "F");
        assertEquals("B C D E F",testee.getTransitiveDependencies("A"));
        assertEquals("C D E F",testee.getTransitiveDependencies("B"));
    }

}